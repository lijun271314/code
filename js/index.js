let that 
class TabChange {
    
    constructor(){
        this.btn = document.getElementById('addBtn')
        this.ul = document.getElementsByTagName('ul')[0]
        this.textArea = document.getElementById('main')
        that = this
        this.init()
        this.add()
    }
    //初始化 
    init(){
        // divs
        this.divs = this.textArea.querySelectorAll('div')
        // lis
        this.lis = this.ul.getElementsByTagName('li')
        // delete
        this.delSpan = this.ul.querySelectorAll('span')
        // 双击编辑元素
        this.h2 = this.ul.querySelectorAll('h2')
        // input框
        this.input = that.ul.querySelectorAll('input')

        // 循环遍历Li
        for(let i = 0;i < that.lis.length; i++){
            that.lis[i].index = i
            that.lis[i].addEventListener('click',this.change)
            that.delSpan[i].addEventListener('click',this.del)
            that.delSpan[i].value = ""
            that.h2[i].addEventListener('dblclick',this.dbl)
            that.h2[i].value = ""
            that.input[i].addEventListener('keyup',this.enterClick)
            that.input[i].addEventListener('blur',this.blurClick)
        }
 
    }
    // 添加
    add(){
        // console.log(this)
        that.btn.addEventListener('click',event=>{
            let HTMLLI = '<li>new tab<span>x</span></li>'
            that.ul.insertAdjacentHTML('beforeend',HTMLLI)

            let HTMLTEXT = '<div>new tab</div>'
            that.textArea.insertAdjacentHTML('beforeend',HTMLTEXT)
             that.init()
        })
        
    }
    // 删除
    del(e){
        // 阻止冒泡
        e.stopPropagation()
        // 获取当前index
        let index = this.parentNode.index
      
         // 高亮
         that.clearClass()        
         if(that.lis.length - 1 >= 1){

            if(index === that.lis.length - 1 ){
                that.lis[index - 1].className = 'activeLi'
                
                that.divs[index - 1].className = 'activeMain'
               
            }else{
                that.lis[index + 1].className = 'activeLi'
               
                that.divs[index + 1].className = 'activeMain'
                
            }
         }


        // 删除
        // console.log(index)
        that.lis[index].remove()
        that.divs[index].remove()



        

        // 初始化
        that.init()
       
    }
    // 切换
    change(){
        that.clearClass()
        this.className = 'activeLi'
        that.divs[this.index].className = 'activeMain'
    }
    // 双击事件
    dbl(event){
        let index = this.parentNode.index
        // console.log(index)
        console.log(event)
        that.input[index].className = "show"
        that.input[index].focus()
        that.h2[index].className = "hide"

    }
    // 回车修改
    enterClick(i,event){
        let index = this.parentNode.index
        if(event.keyCode  === 13){
            that.input[index].className = "hide"
            that.h2[index].className = "show"
        }

        // 保存框内的内容
        that.h2[index].value = that.input[index].value
        that.h2[index].innerHTML0 = that.h2[index].value

        // that.delSpan[index].value = that.divs[i].value 
        // that.divs[i].innerHTML=that.delSpan[index].value
    }

    // 鼠标焦点离开修改
   blurClick(i){
    let index = this.parentNode.index
    // 保存框内的内容
    that.h2[index].value = that.input[index].value
    that.h2[index].innerHTML = that.h2[index].value


    that.input[index].className = "hide"
    that.h2[index].className = "show"
    
    
   }

    // 清除样式
    clearClass(){

        for(let i =0 ;i < that.lis.length -1 ; i++){
        for(let i =0 ;i < that.lis.length ; i++){

            that.lis[i].className= ""
            that.divs[i].className = ""
        }
    }
}
}
const tab = new TabChange()
